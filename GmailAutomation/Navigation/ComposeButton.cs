﻿using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace GmailAutomation
{
    public class ComposeButton
    {
        // Go to 'compose'
        public static void GoTo()
        {
            Driver.Instance.FindElement(By.Id(":2v")).Click();
        }
    }
}
