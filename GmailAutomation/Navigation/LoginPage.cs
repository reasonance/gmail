﻿using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace GmailAutomation
{
    public class LoginPage
    {
        private string email;
        private string password;

        public LoginPage(string email)
        {
            this.email = email;
        }

        public static LoginPage LoginAs(string email)
        {
            return new LoginPage(email);
        }

        public static void GoTo()
        {
            Driver.Instance.Navigate().GoToUrl("http://gmail.com/");
        }
              
        public LoginPage WithPassword(string password)
        {
            this.password = password;
            return this;
        }

        public void Login()
        {
            // Enter the e-mail
            var loginInput = Driver.Instance.FindElement(By.Id("Email"));
            loginInput.SendKeys(email);

            // Click on the button "next"
            var next = Driver.Instance.FindElement(By.Id("next"));
            next.Click();

            // Enter the password
            var passwordInput = Driver.Instance.FindElement(By.Id("Passwd"));
            passwordInput.SendKeys(password);

            // Click on the button "Sign in"
            var loginButton = Driver.Instance.FindElement(By.Id("signIn"));
            loginButton.Click();
        }
    }
}
