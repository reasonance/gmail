﻿using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace GmailAutomation
{
    public class MessagePage
    {
        private string recipient;
        private string subject;
        private string msg;

        public MessagePage(string recipient)
        {
            this.recipient = recipient;

            var rec = Driver.Instance.FindElement(By.Id(":7p"));
            rec.SendKeys(recipient);      
        }

        public static MessagePage WriteMessageTo(string recipient)
        {
            return new MessagePage(recipient);
        }

        public MessagePage WithText(string msg)
        {
            this.msg = msg;

            var text = Driver.Instance.FindElement(By.Id(":8e"));
            text.SendKeys(msg);

            return this;
        }

        public MessagePage WithSubject(string subject)
        {
            this.subject = subject;

            var sub = Driver.Instance.FindElement(By.Id(":79"));
            sub.SendKeys(subject);

            return this;
        }

        public void Send()
        {
            var send = Driver.Instance.FindElement(By.Id(":6z"));
            send.Click();
        }
            
        public MessagePage Discard()
        {
            var trash = Driver.Instance.FindElement(By.Id(":55"));
            trash.Click();
            return this;
        }

        public void UndoDiscard()
        {
            var undo = Driver.Instance.FindElement(By.Id("link_undo"));
            undo.Click();
        }

        public static void OpenFormatMenu()
        {
            var open = Driver.Instance.FindElement(By.Id(":8u"));
            open.Click();
            open.Click();
        }

        public static bool IsMessageSent()
        {
            return Driver.Instance.FindElement(By.Id("link_vsm")).Displayed;
        }

        public static bool IsMessageDiscarded()
        {
            return Driver.Instance.FindElement(By.Id("link_undo")).Displayed;
        }

        public static bool IsDraftUndone()
        {
            return Driver.Instance.FindElement(By.Id(":6o")).Displayed;
        }

        public static bool IsFormatMenuOpened()
        {
            return Driver.Instance.FindElement(By.Id(":9y")).Displayed;
        }

        public static bool IsReactionCorrect()
        {
            return Driver.Instance.FindElement(By.ClassName("Kj-JD")).Displayed;
        }
    }
}