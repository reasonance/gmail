﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GmailAutomation;

namespace GmailTests
{
    [TestClass]
    public class IncorrectEmailTests : BaseTest
    {
        [TestMethod]
        public void incorrect_email()
        {
            MessagePage.WriteMessageTo("!@#$%^&*()@gmail.com").
                WithSubject("Test message").
                WithText("Simple text").Send();
            
            Assert.AreEqual(true, MessagePage.IsReactionCorrect());
        }
    }
}
