﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GmailAutomation;

namespace GmailTests
{
    public class BaseTest
    {
        [TestInitialize]
        public void Init()
        {
            Driver.Initialize();

            // Pre-condition
            LoginPage.GoTo();
            LoginPage.LoginAs("pupkinvasiliy082@gmail.com").
                WithPassword("pupkinvasiliy").Login();
            ComposeButton.GoTo();
        }
        [TestCleanup]
        public void Cleanup()
        {
         //   Driver.Close();
        }
    }
}
