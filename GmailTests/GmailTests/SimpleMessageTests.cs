﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GmailAutomation;

namespace GmailTests
{
    [TestClass]
    public class SimpleMessageTests : BaseTest
    {
        [TestMethod]
        public void user_can_send_a_simple_message()
        {
            MessagePage.WriteMessageTo("kovalenko.vlad97@gmail.com").
                WithSubject("Test message").
                WithText("Simple Text").
                Send();

            Assert.AreEqual(true, MessagePage.IsMessageSent());
        }
    }
}
