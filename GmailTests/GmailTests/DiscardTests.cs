﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GmailAutomation;

namespace GmailTests
{
    [TestClass]
    public class DiscardTests : BaseTest
    {
        [TestMethod]
        public void user_can_discard_a_draft()
        {
            MessagePage.WriteMessageTo("kovalenko.vlad97@gmail.com").
                    WithText("Simple Text").
                    Discard();

            Assert.AreEqual(true, MessagePage.IsMessageDiscarded());
        }

    }
}
