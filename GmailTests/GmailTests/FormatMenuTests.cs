﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GmailAutomation;

namespace GmailTests
{
    [TestClass]
    public class FormatMenuTests : BaseTest
    {
        [TestMethod]
        public void user_can_open_a_format_menu()
        {
            MessagePage.OpenFormatMenu();
            Assert.AreEqual(true, MessagePage.IsFormatMenuOpened());
        }
    }
}
