﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GmailAutomation;

namespace GmailTests
{
    [TestClass]
    public class RestoreTests : BaseTest
    {
        [TestMethod]
        public void user_can_restore_a_draft()
        {
            MessagePage.WriteMessageTo("kovalenko.vlad97@gmail.com").
                    WithText("Simple Text").
                    Discard().UndoDiscard();

            Assert.AreEqual(true, MessagePage.IsDraftUndone());
        }

    }
}
